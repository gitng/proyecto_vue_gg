-----------------------------------------------------------------
Portafolio Gabriel-Gamboa
-----------------------------------------------------------------
Versión: 0.1
Fecha de Creación: 6/1/2021
Autor: Gabriel Gamboa
-----------------------------------------------------------------

Este es un portafolio que se ejecuta en un serve onlien atravez de el framework de Vue.js
Requisitos:

-Poseer Algún tipo de explorador web.
-Tener el XAMPP instalado en tu computadora.
-(Opcional) Tener un compilador de Código por si quieres visualizar 
la códificación como Visual studio code u otro de tu preferencia
-Tener Instalado node.js
-Tener Instalado npm
-Tener Instalado Vue.js

Intrucciones:

Opcion 1:
-Si tienes Visual Studio Code Arratra la carpeta del proyecto al 
Visual estudio o abrela desde la opción archivo Abrir Carpeta y
Seleccionas la Carpeta "proyecto_gamboa_wedapp"
-Luego abres la Terminal del Visual Studio
-Si tienes todo Instalado escribes el comando "npm run serve"
-Esperas a que se inicialice y luego puedes hacer click a la dirreción
o copiarla y pegarla en el buscador, con esos ya puedes ver el proyecto.

Opción 2:

-Primero tenemos que entrar a la terminal para ello podemos usar los siguientes
metodos:

1.Darle al boton de Inicio y escribir en el buscador "cmd" y cuando cargue darle click al programa que tenga dicho nombre.
2.Darle al boton de Inicio ir darle click a la carpeta "programas", luego a la
carpeta "accesorios", por ultimo al programa llamado "simbolo del sistema".

-Ahora dentro de la terminal tenemos que situarnos en la carpeta del proyecto
para ello debemos escribir la ruta en donde se encuentra dicha carpeta por ejemplo: "cd\Documentos\Portafolio".

-Ya dentro de la carpeta si instalaste todo lo mencionando anteriormente
vamos a ejecutar el comando "npm run serve" dejamos que inicialice el 
servidor y luego cuando ya cargue copiamos o hacemos click en la dirección
para que habrá en el navegador.

Advertencia:
-Se necesita una conexión de internet para ejecutar este proyecto.
-La terminal no debe cerrase mientras se está copilando el proyecto
si lo hace el proyecto dejará de mostrarse ya que framework de Vue
trabaja con un servidor a tiempo real y estárias cortado el acceso
al servidor que Vue está haciendo atravez de la terminal.

Construido con:

-Visual Studio Code

Usando Lenguaje como:

-Html5
-Javascript
-Css
-Php

-----------------------------------------------------------------
Cambios:
-----------------------------------------------------------------
Calculadora V.0.1

-Era poco intuitiva para el usuario
-Los campos de validación poseian muchos errores
-No estaba bien estructurado
-No se ejecutaba bien el código

Calculadora V.0.2

-Es mucho más intuitiva para el usuario (tiene un mejor diseño)
-Tiene botones con los cuales puedes interacturar 
-Las operaciones matemáticas ya pueden ejecutarse bien
-No es tan engorroso a la vista

Se a Añadido un Juego de buscaminas con una sección Llamada "Juego"
-----------------------------------------------------------------

-----------------------------------------------------------------
Códigos Agregados
-----------------------------------------------------------------
-Formulario de Registro
-Sección de Inicio
-Sección de Documentación
-Sección de Códigos
-Sección de Buscaminas
-----------------------------------------------------------------

-----------------------------------------------------------------
Errores:
-----------------------------------------------------------------
-Calculadora V.0.1 no copila el código php y muestra por pantalla 
(Aún solucionandose)
-Calculadora V.0.2 Si salta un Error,NaN o Infinite, puedes escribir 
números en el (Aún solucionandose)
-Se tuvo que remover el loader ya que no ejecutava el código de css
-Se tuvo que remover el login porque no conectaba con la Base de Datos
-Los códigos de Javascript aunque funcionan en un html normal aqui no
copilan el código
-A causa de Esto el Formulario y el Juego no Funcionan porque no copila
el Javascript
-----------------------------------------------------------------


-----------------------------------------------------------------
Agradecimientos Especiales
-----------------------------------------------------------------
-Profesor Sergio
-Youtube.com
-Google.com
-----------------------------------------------------------------
