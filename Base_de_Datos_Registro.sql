-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2022 a las 19:43:37
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datosusuarios`
--

CREATE TABLE `datosusuarios` (
  `ID_Persona` int(11) NOT NULL,
  `Nombre` varchar(15) DEFAULT NULL,
  `Apellido` varchar(15) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `Contraseña` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `datosusuarios`
--

INSERT INTO `datosusuarios` (`ID_Persona`, `Nombre`, `Apellido`, `Fecha`, `Correo`, `Contraseña`) VALUES
(10, 'Yuka', 'Frita', '2000-11-03', 'funciona123@gmail.com', 'soyuntuberculo'),
(11, 'Aiuda', 'Noseserproo', '1988-10-16', 'sergionomefunes@gmail.com', 'yatevisergio'),
(12, 'Manolo', 'Romero', '2021-11-03', 'vamosaverquesucede@gmail.com', '$2y$10$/Kvn.bb2jpbeFX4Fv7DaneypwylUIOEJVi7n4pilBycssEBuMgZU2'),
(13, 'willy', 'mania', '2021-11-04', 'ww@gmail.com', '$2y$10$gwHz0ng79YcB0dZntQTOQuzfqCZcu3c2SvkQuefdOuJgP4QLfzmIS'),
(14, 'Jg', 'Maria', '2021-11-04', 'ff@gmail.com', '$2y$10$0BLL9AzjWSzCQWYlcfdLiufO4t/ECndmAz3fp5blpUYVyT30D4PJ.'),
(15, 'safsdf', 'sADFAFDS', '2021-11-03', 'ASD@gmail.com', '$2y$10$S1RXu9SQww5L0QzwYwtv8eDmA2u4phO2pZDFnrNahaYC0mFhCVawy'),
(16, 'penelope', 'garcia', '2021-11-30', 'dsvbf@gmail.com', '$2y$10$e5CtYF05c4rgjIJldjfMHesNjfy/5A7mkcm0hBNdAgVVxFq09jAKi'),
(17, 'qwer', 'qwer', '2021-11-10', 'jose@gmail.com', '$2y$10$vTB3zerMY1mlkItmD51JR.ujsytPKqfwGBv6P3prVNQbSkh2rROE.'),
(18, 'yuka', 'yuka', '2021-11-30', 'sdefe@gmail.com', '$2y$10$Cf3ZHPtsvTd2Tw6/uth1KOJxm.5xHNYGVOc9vcLPFn0C/BLTZUoSe'),
(19, 'f', 'g', '2021-11-04', '12@gmail.com', '$2y$10$2UQsFxU5f/froOAhJXsbmura2.WjeZMCbUEN32OC/cJvXKdgozFxK'),
(20, 'maria', 'yuka', '2021-11-03', 'federico@gmail.com', '$2y$10$nQLrn4T8GwZaScr/DMxx.udT8UofuXyf0xtW9kdA7JTwKGrGekBLy'),
(21, 'f', 'f', '2021-11-27', 'f@gmail.com', '$2y$10$YwLJVIrbwkp7Bdbfeo0wBeFoEecj84pEQgzItQSxcwZ25iqKUVdEG'),
(23, 'Gon', 'Torrealba', '1961-12-12', '2003jg@gmail.com', '$2y$10$6fXSZPQz0GKFF7Grg19n1.z1HvI2gjnnFLT5kGpTLDWtiWdrAsmji');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datosusuarios`
--
ALTER TABLE `datosusuarios`
  ADD PRIMARY KEY (`ID_Persona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `datosusuarios`
--
ALTER TABLE `datosusuarios`
  MODIFY `ID_Persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
